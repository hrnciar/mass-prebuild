#!/usr/bin/python3
""" Retrieve reverse dependencies based on a list of packages

    This module provides facilities in order to calculate the reverse
    dependencies of a provided list of packages. The list is segregated based
    on the architecture and the corresponding distribution's package
    repositories that should be looked at.
    The module can either be used stand-alone (as a plain application) or
    imported into another module.
"""

import argparse
from math import floor
import dnf
import koji
import yaml

from . import VERSION

arch_table = {
    'aarch64': {'repo_arch': 'aarch64', 'search_arch': 'aarch64'},
    'i386': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'i586': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'i686': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'ppc64le': {'repo_arch': 'ppc64le', 'search_arch': 'ppc64le'},
    's390x': {'repo_arch': 's390x', 'search_arch': 's390x'},
    'x86': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'x86_64': {'repo_arch': 'x86_64', 'search_arch': 'x86_64'},
}

METALINKS = {
    'fedora': {
        'base': 'https://mirrors.fedoraproject.org/metalink?',
        'src-repos': [
            'repo=fedora-source-{}&arch=source',
            'repo=updates-released-source-f{}&arch=source',
        ],
        'bin-repos': ['repo=fedora-{}&arch={}', 'repo=updates-released-f{}&arch={}'],
        'koji': 'https://koji.fedoraproject.org/kojihub',
    },
    'epel': {
        'base': 'https://mirrors.fedoraproject.org/metalink?',
        'src-repos': ['repo=epel-source-{}&arch=source'],
        'bin-repos': ['repo=epel-{}&arch={}'],
        'koji': 'https://koji.fedoraproject.org/kojihub',
    },
    'centos-stream': {
        'base': 'https://mirrors.centos.org/metalink?',
        'src-repos': [
            'repo=centos-baseos-source-{}-stream&arch=source&protocol=https,http',
            'repo=centos-crb-source-{}-stream&arch=source&protocol=https,http',
            'repo=centos-appstream-source-{}-stream&arch=source&protocol=https,http',
        ],
        'bin-repos': [
            'repo=centos-baseos-{}-stream&arch={}&protocol=https,http',
            'repo=centos-crb-{}-stream&arch={}&protocol=https,http',
            'repo=centos-appstream-{}-stream&arch={}&protocol=https,http',
        ],
        'koji': 'https://cbs.centos.org/kojihub',
    },
}


def koji_session(distrib='fedora'):
    """Global access to koji session"""
    if not hasattr(koji_session, "sess"):
        koji_session.sess = koji.ClientSession(METALINKS[distrib]['koji'])

    return koji_session.sess


def _dnf_base(distrib='fedora', releasever='rawhide', arch='x86_64', srcbin='all'):
    """DNF base instances"""
    if not hasattr(_dnf_base, "sess"):
        _dnf_base.sess = {}

    if arch not in _dnf_base.sess:
        _dnf_base.sess[arch] = {}

    if srcbin not in _dnf_base.sess[arch]:
        base = dnf.Base()

        # Setup the repositories
        meta_base = METALINKS[distrib]['base']

        repo_arch = arch_table[arch]['repo_arch']

        for src_repo in METALINKS[distrib]['src-repos']:
            idx = METALINKS[distrib]['src-repos'].index(src_repo)
            repo = dnf.repo.Repo(
                'custom_' + arch + '_' + str(idx) + '-source', base.conf
            )
            repo.metalink = meta_base + src_repo.format(releasever)
            base.repos.add(repo)

        for bin_repo in METALINKS[distrib]['bin-repos']:
            idx = METALINKS[distrib]['bin-repos'].index(bin_repo)

            repo = dnf.repo.Repo('custom_' + arch + '_' + str(idx), base.conf)
            repo.metalink = meta_base + bin_repo.format(releasever, repo_arch)
            base.repos.add(repo)

        # By default disable everything
        repos = base.repos.get_matching('*')
        repos.disable()

        if any([srcbin == 'all', srcbin == 'bin']):
            # Enable binaries
            repos = base.repos.get_matching('custom_*')
            repos.enable()

        if any([srcbin == 'all', srcbin == 'src']):
            # Enable sources
            repos = base.repos.get_matching('custom*-source')
            repos.enable()

        base.fill_sack(load_system_repo=False)

        _dnf_base.sess[arch][srcbin] = base

    return _dnf_base.sess[arch][srcbin]


def dnf_base(distrib='fedora', releasever='rawhide', arch='x86_64'):
    """DNF base instances for both binaries and sources"""
    return _dnf_base(distrib, releasever, arch, 'all')


def dnf_base_src(distrib='fedora', releasever='rawhide', arch='x86_64'):
    """DNF base instances for both sources only"""
    return _dnf_base(distrib, releasever, arch, 'src')


def dnf_base_bin(distrib='fedora', releasever='rawhide', arch='x86_64'):
    """DNF base instances for both binaries only"""
    return _dnf_base(distrib, releasever, arch, 'bin')


def get_last_build(package, distrib='fedora', releasever='rawhide', arch='x86_64'):
    """Get the source package name and commit ID from latest version of a
    package.

    This function first retrieves the latest available package matching the
    name provided, and reconstruct the NVR of the corresponding source package.
    Based on this NVR, it asks Koji to give the build information associated to
    it, and extracts the commit ID out of this data.

    Args:
        package: The package to get the tuple from
        distrib: The distribution (fedora, epel, centos ...)
        releasever: The release version for the distribution. e.g for fedora:
        36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
        arch: The architecture for the packages (x86_64, aarch64 ...).

    Returns:
        source name, commit ID
    """
    pkgs = (
        dnf_base(distrib, releasever, arch)
        .sack.query()
        .available()
        .filter(name=package, latest=1)
    )

    committish = releasever
    name = package

    for pkg in list(pkgs):
        nvr = f'{pkg.source_name or pkg.name}-{pkg.version}-{pkg.release}'

        try:
            build_info = koji_session(distrib).getBuild(nvr, strict=True)
            committish = build_info['source'].split('#')[1]
            name = pkg.source_name or pkg.name
            break
        except koji.GenericError:
            print(f'Package {nvr} not found in koji')

    return name, committish


def __get_reverse_deps(
    packages, distrib='fedora', releasever='rawhide', arch='x86_64'
) -> dict:
    """Returns a dict of package reverse dependencies.

    From a list of a given package, request dnf to retrieve the packages that
    require at least one of the elements of this list from its BuildRequires
    field.

    Args:
        packages: The list of packages to get reverse dependencies of.
        distrib: The distribution (fedora, epel, centos ...)
        releasever: The release version for the distribution. e.g for fedora:
        36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
        arch: The architecture for the packages (x86_64, aarch64 ...).

    Returns:
        A dict of packages that depend on the input for their build, on a
        dedicated architecture.
        These packages are either architecture independent (noarch), or specific to
        the architecture requested.
    """
    # pylint: disable= too-many-locals, too-many-statements

    search_arch = arch_table[arch]['search_arch']
    base = dnf_base(distrib, releasever, arch)

    pkg_set = set()

    for pkg_src in packages:
        pkgs = base.sack.query().available().filter(name=pkg_src, latest=1)
        pkg_set.update([f'pkgconfig({p})' for p in pkg_src])

        for pkg in list(pkgs):
            # Reconstruct source package name based on NVR, and use it in order to
            # check if there are packages generated for the required architecture
            bin_packages_noarch = (
                base.sack.query()
                .available()
                .filter(
                    sourcerpm=f'{pkg.name}-{pkg.version}-{pkg.release}.src.rpm',
                    arch='noarch',
                )
            )
            bin_packages = (
                base.sack.query()
                .available()
                .filter(
                    sourcerpm=f'{pkg.name}-{pkg.version}-{pkg.release}.src.rpm',
                    arch=search_arch,
                )
            )

            bin_list = list(bin_packages) + list(bin_packages_noarch)

            for bin_pkg in bin_list:
                pkg_set.add(bin_pkg.name)
                pkg_set.add(f'pkgconfig({bin_pkg.name})')

    # Now that we have the list of binary packages to use as required set,
    # calculate the corresponding reverse dependencies
    base = dnf_base_src(distrib, releasever, arch)

    # Get reverse dependencies based on the sources
    revdeps = base.sack.query().available().filter(requires=pkg_set, latest=1)

    # Get only binary packages so that we can filter per arch
    base = dnf_base_bin(distrib, releasever, arch)

    ret = set()

    ret.update([p.name for p in revdeps])
    print(f'Found {len(ret)} candidate packages.')

    ret = {}
    for pkg in list(revdeps):
        # Don't add package in reverse dependencies if
        # they are already in the main package list
        if pkg.name in pkg_set:
            continue

        if pkg.name in ret:
            continue

        nvr = f'{pkg.name}-{pkg.version}-{pkg.release}'
        # Reconstruct source package name based on NVR, and use it in order to
        # check if there are packages generated for the required architecture
        bin_packages_noarch = (
            base.sack.query()
            .available()
            .filter(
                sourcerpm=f'{nvr}.src.rpm',
                arch='noarch',
            )
        )
        bin_packages = (
            base.sack.query()
            .available()
            .filter(
                sourcerpm=f'{nvr}.src.rpm',
                arch=search_arch,
            )
        )

        bin_list = list(bin_packages) + list(bin_packages_noarch)

        if bin_list:
            ret[pkg.name] = {}
            ret[pkg.name]['deps'] = [dep.name for dep in pkg.regular_requires]
            if list(bin_packages_noarch):
                ret[pkg.name]['arch'] = ['all']
            else:
                ret[pkg.name]['arch'] = [search_arch]

            if not len(ret) % 10:
                print(f'Got {len(ret)} packages for {search_arch}.', end='\r')

    print(f'Got {len(ret)} packages for {search_arch}.')
    return ret


def get_reverse_deps(
    packages,
    distrib='fedora',
    releasever='rawhide',
    arch_set=None,
    depth=0,
    priorities=True,
) -> dict:
    """Returns a dict of package reverse dependencies.

    From a list of given packages, request dnf to retrieve the packages that
    require at least one of the elements of this list from its BuildRequires
    field.
    Multiple architectures can be provided, the returned dictionary has an
    "archs" field listing all supported architectures for a dedicated package.

    The priority is informational, package with priority N likely depends on
    packages with priority N-1, unless a circular dependency is encountered.

    Args:
        packages: The list of packages to get reverse dependencies of.
        distrib: The distribution (fedora, epel, centos ...)
        releasever: The release version for the distribution. e.g for fedora:
            36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
        archs: A set of architectures to look for the packages (x86_64, aarch64 ...).
        depth: The depth of the dependency tree to look for dependencies
            (default: 0, i.e. direct dependencies). Use it with parsimony,
            depth=2 will likely grab the whole distribution.
            e.g.: glibc depth 0: 258 packages, glibc depth 1: 15K packages.

    Returns:
        A dict of packages that depend on the input for their build. The
        dictionary has the following structure:
        ret = {
            'package_name': {
                'src_type': 'distgit',
                'priority': N, # N being any number starting from 0
                'archs': [ ... ],
                'deps': [ ... ]
                },
                ...
            }
    """
    # pylint: disable= too-many-arguments, too-many-locals, too-many-branches

    if arch_set is None:
        arch_set = {'x86_64'}

    ret = {}
    current_depth = 0
    pkg_list = [[packages]]

    if not packages:
        return ret

    while current_depth <= depth:
        pkg_dict = {}
        pkg_list.append([])
        for arch in arch_set:
            if depth > 0:
                print(
                    f'Level {current_depth} depth for {arch} on {distrib} {releasever}'
                )
            pkg_dict[arch] = __get_reverse_deps(
                pkg_list[current_depth], distrib, releasever, arch
            )

            for name, pkg in pkg_dict[arch].items():
                if name in ret:
                    if not any(a in ret[name]['arch'] for a in ['all', arch]):
                        ret[name]['arch'].append(arch)
                else:
                    ret[name] = {}
                    ret[name]['src_type'] = 'distgit'
                    ret[name]['src'] = distrib
                    ret[name]['arch'] = pkg['arch']
                    ret[name]['deps'] = pkg['deps']
                    ret[name]['committish'] = '@last_build'
                    # Set default priority
                    ret[name]['priority'] = 0
                    ret[name]['occurrence'] = 0

                    pkg_list[current_depth + 1].append(name)

                print(f'Collected data for {len(ret)} packages.', end='\r')

            print(f'Collected data for {len(ret)} packages.')

        current_depth += 1

    if not priorities:
        return ret

    # Prepare the arbitrary discriminator to flatten the dependency graph
    print('Prepare discriminator for priorities calculation')
    # consider-using-dict-items: We actually want to traverse the dict twice
    # pylint: disable= consider-using-dict-items
    for name in ret:
        print(f'{floor(list(ret).index(name) * 100 /len(list(ret)))}% done', end='\r')
        for pkg in ret.values():
            if name in pkg['deps']:
                ret[name]['occurrence'] += 1
    # pylint: enable= consider-using-dict-items

    print('100% done')
    print('Setting priorities')
    # Re-calc priority based on a simplified dependency map
    increased_prio = True
    current_prio = 0
    while increased_prio:
        print(f'Pass {current_prio}', end='\r')
        increased_prio = False
        prio_list = [
            name for name, pkg in ret.items() if pkg['priority'] == current_prio
        ]
        for name, pkg in ret.items():
            if pkg['priority'] != current_prio:
                continue
            for dep in pkg['deps']:
                if dep in prio_list:
                    # Break circular dependencies.
                    # Use an arbitrary discriminator to flatten the dependency
                    # graph. The more occurrences can be found in the
                    # dependencies, the highest the priority for this package
                    # is. Identical occurrences means same priority (like a
                    # package being dependent on itself).
                    if ret[dep]['occurrence'] <= pkg['occurrence']:
                        continue

                    pkg['priority'] += 1
                    increased_prio = True

                    break
        current_prio += 1

    print(f'{current_prio} pass done.')
    return ret


def main():
    """Calculate reverse dependencies for packages given in command line

    This function is the entry point for the command line interface to
    calculate the reverse dependencies of a given list of packages.

    The list is either printed to STDOUT or to a file, as requested by the
    user.
    """

    parser = argparse.ArgumentParser(
        description='A package reverse dependency collector'
    )

    parser.add_argument(
        '--verbose', '-V', action='store_true', help='increase output verbosity'
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument(
        '-o', '--output', default='/dev/stdout', help='where to store the list'
    )

    parser.add_argument(
        '--distrib',
        default='fedora',
        help='distribution for the package repository',
    )

    parser.add_argument('-a', '--arch', default='x86_64', help='architecture')
    parser.add_argument('-d', '--depth', default=0, type=int, help='dependency depth')
    parser.add_argument(
        '-r',
        '--releasever',
        default='rawhide',
        help='release version (e.g 36, rawhide ...)',
    )

    parser.add_argument(
        'packages',
        nargs='+',
        type=str,
        action='extend',
        help='list of packages to collect reverse dependencies for',
    )

    args = parser.parse_args()

    if args.verbose:
        pkgs = ', '.join(args.packages)
        print(f'Collecting reverse dependencies for {pkgs}')
        print(f'Result will be provided to {args.output}')

    for pkg in args.packages:
        name, committish = get_last_build(pkg, args.distrib, args.releasever, args.arch)
        print(f'{pkg}:{name} {committish}')

    pkgs = get_reverse_deps(
        args.packages, args.distrib, args.releasever, set([args.arch]), args.depth
    )

    with open(args.output, 'w', encoding='utf-8') as file:
        print(f'{yaml.dump(pkgs)}', file=file)


if __name__ == '__main__':
    main()
