#!/usr/bin/python3
""" COPR back-end implementation

    This module implements the COPR specific functionalities for the mass
    pre-builder.
"""

import os
from pathlib import Path
import subprocess

from copr.v3 import Client, exceptions, config_from_file

from .package import MpbPackage
from .backend import MpbBackend

arch_table = {
    'aarch64': {'fedora': 'aarch64', 'epel': 'aarch64', 'centos-stream': 'aarch64'},
    'i386': {'fedora': 'i386', 'epel': 'x86_64', 'centos-stream': 'x86_64'},
    'i586': {'fedora': 'i386', 'epel': 'x86_64', 'centos-stream': 'x86_64'},
    'i686': {'fedora': 'i386', 'epel': 'x86_64', 'centos-stream': 'x86_64'},
    'ppc64le': {'fedora': 'ppc64le', 'epel': 'ppc64le', 'centos-stream': 'ppc64le'},
    's390x': {'fedora': 's390x', 'epel': 's390x', 'centos-stream': 's390x'},
    'x86': {'fedora': 'i386', 'epel': 'x86_64', 'centos-stream': 'x86_64'},
    'x86_64': {'fedora': 'x86_64', 'epel': 'x86_64', 'centos-stream': 'x86_64'},
}


class MpbCoprPackage(MpbPackage):
    """Mass pre-build copr specific package implementation

    Attributes:
        pkg_id: The package identifier for cross referencing between packages
        name: The name of the package
        base_build_id:
            The identifier of the mass pre-build the package is built on
        build_id: Build ID on the infrastructure
        build_status: A dictionary of build status per supported arch
        pkg_type: The type of the package, between main and reverse dependency
        skip_archs: The set of non-supported arch (to ease filtering)
        src_type: The source type (distgit, url, file, ...)
        src: The actual source
        committish: The tag, branch, commit ID ... For distgits ant gits
        priority: The priority for build batches
        after_pkg: A package ID we need to wait for to be able to build
        with_pkg: A package ID we build together with

    """

    def _build_distgit(self, with_id=0, after_id=0):
        """Infrastructure specific build method"""
        accepted_chroots = []
        for arch in self.archs:
            if arch in arch_table:
                accepted_chroots.append(
                    f'{self._owner.chroot}-{arch_table[arch][self._owner.distgit]}'
                )

        opts = {
            'timeout': '108000',
            'chroots': accepted_chroots,
            'background': True,
        }

        # COPR interface forbids to provide both values simultaneously (even if
        # one as a 0 value). We therefore need to choose one.
        # The with_id as the highest priority, as after_id is always set if
        # priority is non-0 and with_id is reset whenever after_id is modified.
        # See backend->_populate_packages for more details.
        if with_id:
            opts['with_build_id'] = with_id
        else:
            if after_id:
                opts['after_build_id'] = after_id

        try:
            pkg_build = self._owner.client.build_proxy.create_from_distgit(
                self._owner.client.config['username'],
                self._owner.name,
                self.src_pkg_name,
                committish=self.committish,
                distgit=self.src,
                buildopts=opts,
            )
        except exceptions.CoprRequestException:
            # We assume that COPR complained that the batch we try to associate
            # this build with is already done, which leads to a request
            # exception. The simplest way to solve that is to reset any batch
            # association for this build.
            if 'after_build_id' in opts:
                opts['after_build_id'] = 0
            if 'with_build_id' in opts:
                opts['with_build_id'] = 0
            pkg_build = self._owner.client.build_proxy.create_from_distgit(
                self._owner.client.config['username'],
                self._owner.name,
                self.src_pkg_name,
                committish=self.committish,
                distgit=self.src,
                buildopts=opts,
            )

        self.build_id = pkg_build['id']
        self._update_package()

    def _build_file(self, with_id=0, after_id=0):
        """Infrastructure specific build method"""
        accepted_chroots = []
        for arch in self.archs:
            if arch in arch_table:
                accepted_chroots.append(
                    f'{self._owner.chroot}-{arch_table[arch][self._owner.distgit]}'
                )

        opts = {
            'timeout': '108000',
            'chroots': accepted_chroots,
            'background': True,
        }

        if after_id:
            opts['after_build_id'] = after_id
        else:
            if with_id:
                opts['with_build_id'] = with_id

        try:
            pkg_build = self._owner.client.build_proxy.create_from_file(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                buildopts=opts,
            )
        except exceptions.CoprRequestException:
            if 'after_build_id' in opts:
                opts['after_build_id'] = 0
            if 'with_build_id' in opts:
                opts['with_build_id'] = 0
            pkg_build = self._owner.client.build_proxy.create_from_file(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                buildopts=opts,
            )

        self.build_id = pkg_build['id']
        self._update_package()

    def _build_git(self, with_id=0, after_id=0):
        """Infrastructure specific build method"""
        accepted_chroots = []
        for arch in self.archs:
            if arch in arch_table:
                accepted_chroots.append(
                    f'{self._owner.chroot}-{arch_table[arch][self._owner.distgit]}'
                )

        opts = {
            'timeout': '108000',
            'chroots': accepted_chroots,
            'background': True,
        }

        if after_id:
            opts['after_build_id'] = after_id
        else:
            if with_id:
                opts['with_build_id'] = with_id

        try:
            pkg_build = self._owner.client.build_proxy.create_from_scm(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                self.committish,
                buildopts=opts,
            )
        except exceptions.CoprRequestException:
            if 'after_build_id' in opts:
                opts['after_build_id'] = 0
            if 'with_build_id' in opts:
                opts['with_build_id'] = 0
            pkg_build = self._owner.client.build_proxy.create_from_scm(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                self.committish,
                buildopts=opts,
            )

        self.build_id = pkg_build['id']
        self._update_package()

    def _build_script(self, with_id=0, after_id=0):
        """Infrastructure specific build method"""
        return

    def _build_url(self, with_id=0, after_id=0):
        """Infrastructure specific build method"""
        accepted_chroots = []
        for arch in self.archs:
            if arch in arch_table:
                accepted_chroots.append(
                    f'{self._owner.chroot}-{arch_table[arch][self._owner.distgit]}'
                )

        opts = {
            'timeout': '108000',
            'chroots': accepted_chroots,
            'background': True,
        }

        if after_id:
            opts['after_build_id'] = after_id
        else:
            if with_id:
                opts['with_build_id'] = with_id

        try:
            pkg_build = self._owner.client.build_proxy.create_from_url(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                buildopts=opts,
            )
        except exceptions.CoprRequestException:
            if 'after_build_id' in opts:
                opts['after_build_id'] = 0
            if 'with_build_id' in opts:
                opts['with_build_id'] = 0
            pkg_build = self._owner.client.build_proxy.create_from_url(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                buildopts=opts,
            )

        self.build_id = pkg_build['id']
        self._update_package()

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        status = 'pending'

        try:
            build = self._owner.client.build_proxy.get(self.build_id)
            status = build['state']
        except exceptions.CoprNoResultException:
            try:
                pkg = self._owner.client.package_proxy.get(
                    self._owner.client.config['username'],
                    self._owner.name,
                    self.src_pkg_name,
                    with_latest_build=True,
                )
                if pkg['builds']['latest']['id']:
                    status = pkg['builds']['latest']['state']
                    self.build_id = pkg['builds']['latest']['id']
            except exceptions.CoprNoResultException:
                pass

        if status in ['importing', 'pending', 'waiting']:
            self.build_status = 'PENDING'

        if status in ['starting', 'running']:
            self.build_status = 'RUNNING'

        if status in ['succeeded', 'forked', 'skipped']:
            self.build_status = 'SUCCESS'

        if status in ['failed', 'canceled']:
            if check_needed:
                self.build_status = 'CHECK_NEEDED'
            else:
                self.build_status = 'FAILED'

    def cancel(self):
        """Infrastructure specific cancel method"""
        if not self.build_id:
            return

        try:
            if self.build_status not in ['SUCCESS', 'FAILED', 'FREE']:
                self._owner.client.build_proxy.cancel(self.build_id)
        except exceptions.CoprNoResultException:
            pass
        except exceptions.CoprRequestException:
            pass

        if self.build_status != 'SUCCESS':
            self.build_status = 'FAILED'

    def collect_data(self, dest):
        """Infrastructure specific data collector method"""
        print(f'Collecting data for {self.src_pkg_name:<50s}', end='\r')
        accepted_chroots = []
        for arch in self.archs:
            if arch in arch_table:
                accepted_chroots.append(
                    f'{self._owner.chroot}-{arch_table[arch][self._owner.distgit]}'
                )

        build = self._owner.client.build_proxy.get(self.build_id)
        base_len = len(os.path.split(build.repo_url))
        chroots = self._owner.client.build_chroot_proxy.get_list(self.build_id)

        print(f'Accepted chroots: {accepted_chroots} {self.archs}')
        for chroot in chroots:
            if chroot.name not in accepted_chroots:
                print(f'Skipping {chroot.name}')
                continue

            if not chroot.result_url:
                chroot.result_url = f'{build.repo_url}/srpm-builds/{self.build_id:08}/'

            cmd = [
                'wget',
                '-r',
                '-nH',
                '--no-parent',
                '--reject',
                '"index.html*"',
                '-e',
                'robots=off',
                '--no-verbose',
                '--progress=bar',
            ]
            cmd.extend(
                [
                    '-P',
                    os.path.join(
                        dest,
                        self._owner.name + "/" + chroot.name + "/" + self.src_pkg_name,
                    ),
                ]
            )
            cmd.extend(['--cut-dirs', str(base_len + 2)])
            cmd.append(chroot.result_url)
            subprocess.call(cmd)

    def clean(self):
        """Infrastructure specific clean method"""
        try:
            if self.build_id:
                self._owner.client.package_proxy.delete(
                    self._owner.client.config['username'],
                    self._owner.name,
                    self.src_pkg_name,
                )
                self.build_id = 0
                self.build_status = 'FREE'
        except exceptions.CoprNoResultException:
            self.build_id = 0
            self.build_status = 'FREE'


class MpbCoprBackend(MpbBackend):
    """Mass pre-build COPR back-end

        Implement COPR specific functionalities.

    Attributes:
        client: The COPR client instance

    """

    def __init__(self, config):
        """Call the super class, and initialize the COPR client"""
        super().__init__(config)
        copr_conf = config_from_file()

        if 'copr' in config:
            if 'config' in config['copr']:
                path = Path(config['copr']['config'])
                if path.is_file():
                    print(f'Using {str(path)} for copr config.')
                    copr_conf = config_from_file(str(path))

        self.client = Client(copr_conf)
        # Retry to connect to remote server 5 times at least.
        # Default value is 1, user may provide a different value in its copr
        # config file
        self.client.config['connection_attempts'] = self.client.config.get(
            'connection_attempts', 5
        )

    def _get_package_class(self):
        """Return the package class to be used internally"""
        return MpbCoprPackage

    def prepare(self):
        """Setup the COPR project and call super class method"""
        accepted_chroots = []
        for arch in self.archs:
            if arch in arch_table:
                accepted_chroots.append(
                    f'{self.chroot}-{arch_table[arch][self.distgit]}'
                )

        # Create a new project in COPR for this mass rebuild
        try:
            self.client.project_proxy.add(
                self.client.config['username'],
                self.name,
                accepted_chroots,
                unlisted_on_hp=True,
                appstream=False,
            )
        except exceptions.CoprRequestException as exc:
            if exc.result['error'].startswith('name:'):
                pass
            else:
                raise

        super().prepare()

    def clean(self):
        """Execute super class method and destroy COPR project"""
        super().clean()

        try:
            if self.verbose >= 1:
                print(f'Deleting project {self.name}')
            self.client.project_proxy.delete(self.client.config['username'], self.name)
        except exceptions.CoprNoResultException as exc:
            print(exc)
        except exceptions.CoprRequestException:
            pass
