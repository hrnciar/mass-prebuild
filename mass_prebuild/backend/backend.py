#!/usr/bin/python3
""" Generic back-end implementation

    This module provides the main functionalities for the mass pre-builder. The
    infrastructure back-ends should only implement the infrastructure specific
    actions, the whole logic is centralized here.
"""

from operator import itemgetter, attrgetter

import os
import sys
import time
import threading

import yaml

import __main__

from .db import MpbDb, authorized_status, authorized_archs, all_archs
from ..whatrequires import get_reverse_deps
from .package import MAIN_PKG, REV_DEP_PKG


def spinning_cursor():
    """Yields a spinning cursor"""
    while True:
        for cursor in '|/—\\':
            yield cursor


class MpbBackend:
    """Mass pre-build generic back-end

        This class implements generic functionalities for the back-ends. The
        whole logic must be in here, while only infrastructure specific
        implementation should go in the dedicated back-ends.

    Attributes:
        verbose: How much this thing is talking
        build_id: ID of this build
        name: Name of this build
        base_build_id: ID of the main build if this is a checker build
        checker_build: Reference to the checker build if this is a main build
        status: Current status of the build
        should_stop: Agents should stop working
        archs: Set of supported architectures
        packages: List of packages to be built
        chroot: Base of the chroots to be used
        stage: Currently executed stage
    """

    # pylint: disable = too-many-instance-attributes

    def __init__(self, config):
        """Create a new mass pre-build back-end entity

            If a build ID is given, the back-end data is retrieved from the
            database. Otherwise, the back-end is created out of the provided
            configuration.
        Args:
            config: The configuration to be used to create the back-end
        """
        self._database = MpbDb(path=config['database'], verbose=config['verbose'])
        self._archs = set()
        self._status = 'FREE'
        self._stage = 0

        self._spinner = spinning_cursor()

        self.verbose = config['verbose']
        self.packages = []
        self._build_list = []
        self._watch_list = []
        self._check_list = []
        self.should_stop = False
        self.checker_build = None

        self._build_event = None
        self._watch_event = None
        self._check_event = None

        self._build_thread = None
        self._watch_thread = None
        self._check_thread = None

        if config['base_build_id'] <= 0:
            config['base_build_id'] = 0

        if config['build_id'] <= 0:
            config['build_id'] = 0
            self.build_id = self._database.new_build(config)
        else:
            self.build_id = config['build_id']

        build_data = self._database.build_by_id(self.build_id)

        self.archs = yaml.safe_load(build_data['archs'])

        self._stages = [
            self.prepare,
            self.check_prepare,
            self.build,
            self.check_build,
            self.collect_data,
        ]

        self.status = 'PENDING'

        if config['stage'] >= 0:
            self.stage = config['stage']
        else:
            self.stage = build_data['stage']

        self.name = build_data['name']

        self.base_build_id = build_data['base_build_id']

        self._data = build_data['data']

        self.chroot = build_data['chroot']

        self.distgit, self._committish, self.releasever = self._gitnbranch()

        self._pkg_class = self._get_package_class()
        self._init_packages(config)

    def _get_package_class(self):
        """Return the package class to be used internally"""
        raise NotImplementedError('Package class must be set')

    def _init_packages(self, config):
        """Populate packages"""
        # pylint: disable = too-many-branches
        try:
            pkgs = self._database.packages_by_base_build_id(self.build_id)
            for pkg in pkgs:
                self._populate_packages(pkg['pkg_id'], pkg)
            if all([pkgs, self.verbose > 1]):
                print('')
        except KeyboardInterrupt:
            self.should_stop = True
            return

        if self.stage < self._stages.index(self.build):
            ptype = MAIN_PKG
        else:
            ptype = REV_DEP_PKG

        for pkg in [p for p in self.packages if p.pkg_type == ptype]:
            if pkg.build_id:
                self._watch_list.append(pkg)
            else:
                self._build_list.append(pkg)

        if self.packages:
            return

        self._populate_packages(pkg_dict=config['packages'])

        if 'list' in config['revdeps'] and config['revdeps']['list']:
            try:
                self._populate_packages(
                    pkg_dict=config['revdeps']['list'], pkg_type=REV_DEP_PKG
                )
            except KeyboardInterrupt:
                print('')
                print('Package list will be corrupted.')
                self.should_stop = True
                return

        if 'append' in config['revdeps']:
            try:
                self._populate_packages(
                    pkg_dict=config['revdeps']['append'], pkg_type=REV_DEP_PKG
                )
            except KeyboardInterrupt:
                print('')
                print('Package list will be corrupted.')
                self.should_stop = True
                return

    _chroot_strings = {
        'centos-stream': {'*': 'c{}'},
        'fedora': {'rawhide': '{}', 'eln': 'rawhide', '*': 'f{}'},
        'epel': {'*': 'el{}'},
        'mageia': {'cauldron': '{}', '*': '{}'},  # To be checked
        'openmandriva': {'cooker': 'master', 'rolling': '{}'},  # To be checked
        'opensuse': {
            'leap-15.2': '{}',
            'leap-15.3': '{}',
            'tumbleweed': '{}',
        },  # To be checked
        'oraclelinux': {'*': '{}'},  # To be checked
    }

    def _gitnbranch(self):
        """Returns a tuple containing the default values for the distgit, the branch,
        and the release version corresponding to the build chroot.
        """
        for git, branch_names in self._chroot_strings.items():
            if self.chroot.startswith(git):
                # Chroots needs to be changed from list to a unique string
                releasever = self.chroot.replace(f'{git}-', '', 1)
                for bname, bformat in branch_names.items():
                    if bname in releasever or bname == '*':
                        branch = bformat.format(releasever)
                        break

                return git, branch, releasever

        return None, None, None

    def _populate_packages(self, pkg_id=0, pkg_dict=None, pkg_type=MAIN_PKG):
        """Populate the package list

            The list is either created from the given dictionary, or from the
            ID of the package given. The dictionary is ignored in the later
            case.
        Args:
            pkg_id: (optional) package ID to retrieve from the database
            pkg_dict: (optional) package data to use to populate the list
            pkg_type: (optional) type for the packages between main package and
            reverse dependency
        """
        # pylint: disable = too-many-branches
        if pkg_id:
            if self.verbose > 1:
                print(f'Populating package for {pkg_dict["name"]}', end='\r')
            self.packages.append(self._pkg_class(self._database, self, pkg_id=pkg_id))
            return

        if pkg_dict is None:
            return

        if not isinstance(pkg_dict, dict):
            return

        pkg_list = []
        for name, pkg in pkg_dict.items():
            # Set default values
            if 'priority' not in pkg:
                pkg['priority'] = 0
            if 'skip_archs' not in pkg:
                pkg['skip_archs'] = None
            if 'src_type' not in pkg:
                pkg['src_type'] = 'distgit'
            if 'src' not in pkg:
                pkg['src'] = self.distgit
            if 'committish' not in pkg:
                pkg['committish'] = None

            pkg['name'] = name
            pkg_list.append(pkg)

        pkg_list = sorted(pkg_list, key=itemgetter('priority', 'name'))

        after_id = 0
        with_id = 0
        last_priority = 0

        for pkg in pkg_list:
            print(f'Populating package list with {pkg["name"]:<50s}', end='\r')
            if pkg['priority'] != last_priority:
                after_id = with_id
                with_id = 0
                last_priority = pkg['priority']
            else:
                after_id = 0

            self.packages.append(
                self._pkg_class(
                    self._database,
                    self,
                    name=pkg['name'],
                    pkg_type=pkg_type,
                    skip_archs=pkg['skip_archs'],
                    src_type=pkg['src_type'],
                    src=pkg['src'],
                    committish=pkg['committish'],
                    priority=pkg['priority'],
                    after_pkg=after_id,
                    with_pkg=with_id,
                )
            )

            with_id = self.packages[-1].pkg_id

        if pkg_list:
            print('')

    def get_package(self, name='', pkg_id=0):
        """Get a package by its name or ID"""
        for pkg in self.packages:
            if all([pkg.name == name, name != '']):
                return pkg
            if all([pkg.pkg_id == pkg_id, pkg_id]):
                return pkg

        return None

    def start_agents(self):
        """Start worker agents"""
        self._build_event = threading.Event()
        self._build_thread = threading.Thread(target=self._build_agent)
        self._build_thread.start()

        self._watch_event = threading.Event()
        self._watch_thread = threading.Thread(target=self._watch_agent)
        self._watch_thread.start()

        self._check_event = threading.Event()
        self._check_thread = threading.Thread(target=self._check_agent)
        self._check_thread.start()

    def stop_agents(self, join=True):
        """Stop worker agents if they exist"""
        self.should_stop = True

        if self.base_build_id:
            return

        if self.checker_build is not None:
            self.checker_build.stop_agents()

        if self._build_event is not None:
            self._build_event.set()
        if self._watch_event is not None:
            self._watch_event.set()
        if self._check_event is not None:
            self._check_event.set()

        if not join:
            return

        if self._build_thread is not None:
            self._build_thread.join()
        if self._watch_thread is not None:
            self._watch_thread.join()
        if self._check_thread is not None:
            self._check_thread.join()

    def _build_agent(self):
        """Build agent, in charge of queuing builds in the infrastructure"""
        # pylint: disable = too-many-branches
        while not self.should_stop:
            if self._build_list:
                self._build_event.wait(5)
            else:
                self._build_event.wait()

            if self.should_stop:
                break

            self._build_event.clear()

            pkg_list = sorted(self._build_list, key=attrgetter('priority', 'name'))
            if pkg_list:
                prio = next(iter(pkg_list)).priority
            else:
                prio = 0

            for pkg in [p for p in pkg_list if p.priority == prio]:
                with_id = 0
                after_id = 0

                if pkg.with_pkg:
                    with_id = self.get_package(pkg_id=pkg.with_pkg).build_id
                    if not with_id:
                        continue

                if pkg.after_pkg:
                    after_id = self.get_package(pkg_id=pkg.after_pkg).build_id
                    if not after_id:
                        continue

                # Any previous build is replaced by this one
                if pkg.build_id:
                    pkg.cancel()
                    pkg.build_status = 'PENDING'
                pkg.build(with_id, after_id)
                self._build_list.remove(pkg)
                self._watch_list.append(pkg)

                if self.should_stop:
                    break

            self._watch_event.set()

    def _watch_agent(self):
        """Watch ongoing builds"""
        while not self.should_stop:
            if self._watch_list:
                self._watch_event.wait(5)
            else:
                self._watch_event.wait()

            if self.should_stop:
                break

            self._watch_event.clear()

            pkg_list = sorted(self._watch_list, key=attrgetter('priority', 'name'))
            if pkg_list:
                prio = next(iter(pkg_list)).priority
            else:
                prio = 0

            for pkg in [p for p in pkg_list if p.priority == prio]:
                if pkg.build_status in ['FAILED', 'SUCCESS']:
                    self._watch_list.remove(pkg)
                    continue

                if pkg.build_status == 'CHECK_NEEDED':
                    self._watch_list.remove(pkg)
                    self._check_list.append(pkg)
                    continue

                # Checking status can be time consuming,
                # check if we shouldn't break the loop
                if self.should_stop:
                    break

                pkg.check_status(pkg.pkg_type == REV_DEP_PKG)

            self._check_event.set()

    def _init_checker_build(self):
        """Create the checker build instance"""
        if self.checker_build is not None:
            return

        if self.base_build_id != 0:
            return

        config = {}
        config['database'] = self._database.path
        config['name'] = f'{self.name}.checker'
        config['base_build_id'] = self.build_id
        config['build_id'] = 0
        config['packages'] = {}
        config['revdeps'] = {}
        config['stage'] = 0
        config['status'] = 'PENDING'
        config['verbose'] = -1
        config['archs'] = self.archs
        config['chroot'] = self.chroot
        config['data'] = ''

        exists = self._database.build_by_base_id(self.build_id)
        if exists is not None:
            config['build_id'] = exists['build_id']

        self.checker_build = self.__class__(config)
        self.checker_build.prepare()

    def _check_agent(self):
        """Initiate check builds for failed packages and watch on their status"""
        self._init_checker_build()

        while not self.should_stop:
            if self._check_list:
                self._check_event.wait(5)
            else:
                self._check_event.wait()

            if self.should_stop:
                break

            self._check_event.clear()

            to_remove = []
            for pkg in self._check_list:
                # Checking status can be time consuming,
                # check if we shouldn't break the loop
                if self.should_stop:
                    break

                chk_pkg = self.checker_build.get_package(name=pkg.name)

                if chk_pkg is None:
                    self.checker_build.packages.append(
                        self._pkg_class(
                            self._database,
                            self.checker_build,
                            name=pkg.name,
                            pkg_type=pkg.pkg_type,
                            skip_archs=pkg.skip_archs,
                            src_type=pkg.src_type,
                            src=pkg.src,
                            committish=pkg.committish,
                        )
                    )
                    chk_pkg = self.checker_build.get_package(name=pkg.name)
                    chk_pkg.build()

                chk_pkg.check_status(check_needed=False)

                if chk_pkg.build_status == 'FAILED':
                    pkg.build_status = 'UNCONFIRMED'
                    to_remove.append(pkg)

                if chk_pkg.build_status == 'SUCCESS':
                    pkg.build_status = 'FAILED'
                    to_remove.append(pkg)

            for pkg in to_remove:
                self._check_list.remove(pkg)

    def report_build_status(self, pkg_type=MAIN_PKG):
        """Print a simple report on the build status per package and give an
        overall status of the build

        Args:
            pkg_type:

        Returns:
            True if all package builds are done, False otherwise

        """
        done = 0
        pending = 0
        running = 0
        success = 0
        check_needed = 0
        failed = 0
        unconfirmed = 0

        packages = [p for p in self.packages if p.pkg_type == pkg_type]

        for pkg in packages:
            if pkg.build_status not in ['FREE', 'PENDING', 'RUNNING', 'CHECK_NEEDED']:
                done += 1
            if pkg.build_status in ['RUNNING']:
                running += 1
            if pkg.build_status in ['FREE', 'PENDING']:
                pending += 1
            if pkg.build_status in ['SUCCESS']:
                success += 1
            if pkg.build_status in ['FAILED']:
                failed += 1
            if pkg.build_status in ['UNCONFIRMED']:
                unconfirmed += 1
            if pkg.build_status in ['CHECK_NEEDED']:
                check_needed += 1

        if self.should_stop:
            return False

        if len(packages):
            print(f'Build status: {next(self._spinner)}')
            print(f'\t{done} out of {len(packages)} builds are done.')
            print(f'\tPending: {pending:<10}')
            print(f'\tRunning: {running:<10}')
            print(f'\tSuccess: {success:<10}')
            print(f'\tUnder check: {check_needed:<10}')
            print(f'\tManual confirmation needed: {unconfirmed:<10}')
            print(f'\tFailed: {failed:<10}')

            if done < len(packages):
                return False

        return True

    def prepare(self):
        """Prepare the infrastructure with the main packages"""
        self.stage = self._stages.index(self.prepare)
        if self.verbose >= 1:
            print(
                f'Executing stage {self.stage} ({self._stages[self.stage].__name__ })'
            )

        if self.verbose >= 3:
            plist = ', '.join([p.name for p in self.packages if p.pkg_type == MAIN_PKG])
            print(f'Preparing mass rebuild {self.name} on {self.chroot} for:')
            print(f'{", ".join(plist)}')

        self.status = 'PENDING'

        if not self.base_build_id:
            self._build_list += [p for p in self.packages if p.pkg_type == MAIN_PKG]
            self._build_event.set()

            self.status = 'RUNNING'

        if self.verbose > 0:
            print(f'Prepared build {self.name} (ID: {self.build_id})')

    def check_prepare(self):
        """Check if the main packages are being built without errors and print
        a simple report"""
        self.stage = self._stages.index(self.check_prepare)
        if self.verbose >= 1:
            print(f'Executing stage {self.stage} ({self._stages[self.stage].__name__})')

        if self.verbose >= 0:
            app = os.path.basename(__main__.__file__)
            print(f'Checking build for {self.name} (ID: {self.build_id})')
            print('You can now safely interrupt this stage')
            print('Restart it later using one of the following commands:')
            print(f'"{app} --buildid {self.build_id}"')
            print(f'"{app} --buildid {self.build_id} --stage {self.stage}"')

        try:
            while not any([self.report_build_status(), self.should_stop]):
                time.sleep(1)
                if not self.should_stop:
                    sys.stdout.write('\x1b[1A' * 8)
        except KeyboardInterrupt:
            self.should_stop = True
            return

        for pkg in [p for p in self.packages if p.pkg_type == MAIN_PKG]:
            if pkg.build_status == 'FAILED':
                self.status = 'FAILED'
                print('Preparation stage failed.')
                self.collect_data()
                break

    def build(self):
        """Calculate the reverse dependencies if they are not already provided
        and queue them for building"""
        self.stage = self._stages.index(self.build)
        if self.verbose >= 1:
            print(
                f'Executing stage {self.stage} ({self._stages[self.stage].__name__ })'
            )

        if self.verbose >= 3:
            plist = ', '.join(
                [p.name for p in self.packages if p.pkg_type == REV_DEP_PKG]
            )
            print(f'Starting mass rebuild {self.name} on {self.chroot} for:')
            print(f'{", ".join(plist)}')

        if not [p for p in self.packages if p.pkg_type == REV_DEP_PKG]:
            if self.verbose >= 1:
                print('Calculating reverse dependencies.')
            try:
                rev_deps = get_reverse_deps(
                    [p.name for p in self.packages if p.pkg_type == MAIN_PKG],
                    self.distgit,
                    self.releasever,
                    self.archs,
                )
            except KeyboardInterrupt:
                print('')
                self.should_stop = True
                return

            try:
                self._populate_packages(pkg_dict=rev_deps, pkg_type=REV_DEP_PKG)
            except KeyboardInterrupt:
                print('')
                print('Package list will be corrupted.')
                self.should_stop = True
                return

        self.status = 'RUNNING'

        self._build_list += [p for p in self.packages if p.pkg_type == REV_DEP_PKG]
        self._build_event.set()

    def check_build(self):
        """Check the build status for the reverse dependencies and print a
        simple report"""
        self.stage = self._stages.index(self.check_build)
        if self.verbose >= 1:
            print(
                f'Executing stage {self.stage} ({self._stages[self.stage].__name__ })'
            )

        if self.verbose >= 0:
            app = os.path.basename(__main__.__file__)
            print(f'Checking build for {self.name} (ID: {self.build_id})')
            print('You can now safely interrupt this stage')
            print('Restart it later using one of the following commands:')
            print(f'"{app} --buildid {self.build_id}"')
            print(f'"{app} --buildid {self.build_id} --stage {self.stage}"')
        try:
            while not any([self.report_build_status(REV_DEP_PKG), self.should_stop]):
                time.sleep(1)
                if not self.should_stop:
                    sys.stdout.write('\x1b[1A' * 8)
        except KeyboardInterrupt:

            self.should_stop = True

    def collect_data(self):
        """For each packages, collect data from the remote servers"""
        self.stage = self._stages.index(self.collect_data)
        if self.verbose >= 1:
            print(f'Executing stage {self.stage} ({self._stages[self.stage].__name__})')

        for pkg in self.packages:
            if pkg.build_status in ['FAILED', 'CHECK_NEEDED', 'UNCONFIRMED']:
                pkg.collect_data(self._data)

        self.status = 'COMPLETED'

    def cancel(self):
        """Cancel build of checker build packages and owned packages"""
        self._init_checker_build()

        if self.checker_build:
            print('Cancel checker\'s packages\' builds')
            self.checker_build.cancel()

        try:
            for pkg in self.packages:
                pkg.cancel()
                pos = self.packages.index(pkg) + 1
                size = len(self.packages)
                print(f'Canceled package build {pos}/{size}: {pkg.name:<50s}', end='\r')
            if self.packages:
                print('')
        except KeyboardInterrupt:
            if self.packages:
                print('')
            return

    def clean(self):
        """Cancel builds, clean packages on the infrastructure and remove the
        remote project
        """
        self._init_checker_build()
        self.status = 'FREE'

        if self.checker_build:
            print('Cleaning checker build')
            self.checker_build.clean()

        try:
            for pkg in self.packages:
                pkg.cancel()
                pkg.clean()
                pos = self.packages.index(pkg) + 1
                size = len(self.packages)
                print(f'Cleaned package {pos}/{size}: {pkg.name:<50s}', end='\r')
            if self.packages:
                print('')
        except KeyboardInterrupt:
            if self.packages:
                print('')
            return

    def execute_stage(self, start=-1):
        """Execute a given stage or the last known one"""
        if start != -1:
            self.stage = start

        if self.stage < len(self._stages):
            self._stages[self.stage]()

    @property
    def archs(self):
        """Returns the set of supported archs"""
        return self._archs

    @archs.setter
    def archs(self, new_arch):
        """Set the set of supported archs

            The set of unsupported archs is automatically updated by removing
            values from this supported arch set.
        Args:
            new_arch: The set to set

        Raises:
            TypeError: The input is not a set
            ValueError: The input contains unsupported values
        """
        if 'all' in new_arch:
            self._archs = all_archs.copy()
            return

        if not isinstance(new_arch, set):
            raise TypeError('Set of arch must be given')

        if not all(elem in authorized_archs for elem in new_arch):
            raise ValueError(f'Archs should be in {authorized_archs}')

        self._archs = new_arch.copy()

    @property
    def status(self):
        """Return overall build status"""
        return self._status

    @status.setter
    def status(self, new_status):
        """Set overall build status"""
        if new_status in authorized_status:
            self._status = new_status
        else:
            raise ValueError(f'status value should be in {authorized_status}')

        self._database.set_state(self.build_id, new_status)

    @property
    def stage(self):
        """Return current stage"""
        return self._stage

    @stage.setter
    def stage(self, new_stage):
        """Set new stage and store it in the database"""
        if new_stage < len(self._stages):
            self._stage = new_stage
        else:
            raise ValueError(
                f'Given stage is out of bounds (max: {len(self._stages)}).'
            )

        self._database.set_stage(self.build_id, new_stage)

    def next_stage(self):
        """Set current stage to the next valid value, or stop the process"""
        if self.should_stop:
            return

        try:
            self.stage += 1
        except ValueError:
            self.should_stop = True

    def __str__(self):
        """Internal string conversion"""
        return str(self.__class__) + ': ' + str(self.__dict__)

    def is_completed(self):
        """Return True if the current status is either FAILED or COMPLETED or
        if the process should stop"""
        return any([self.status in ['FAILED', 'COMPLETED'], self.should_stop])
