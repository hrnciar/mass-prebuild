#!/usr/bin/python3
""" Mass pre-builder main interface

    The mass pre-builder is a set of tools meant to help the user to build its
    package and any reverse dependencies from it. The overall goal is to assess
    if a change in the main package obviously affects negatively its reverse
    dependencies. The infrastructure will not only build the packages but also
    execute the associated tests. If any error is found during the process, the
    user is informed from this failure.
"""

import argparse
import logging
from pathlib import Path
import sys
import threading
import yaml

from . import VERSION
from .backend import copr, dummy
from .backend.db import authorized_archs

LOGGER = None
CURRENT_BACKEND = None

BACKENDS = {'dummy': dummy.DummyBackend, 'copr': copr.MpbCoprBackend}


def handle_thread_exception(args):
    """Redirect thread exception to the general exceptions"""
    if args.exc_type == SystemExit:
        return
    sys.excepthook(args.exc_type, args.exc_value, args.exc_traceback)


def handle_exception(exc_type, exc_value, exc_traceback):
    """Log exception into a file to ease debugging"""
    if exc_type == SystemExit:
        return

    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    LOGGER.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    if CURRENT_BACKEND is not None:
        CURRENT_BACKEND.stop_agents(False)

    print('Stopping due to Uncaught exception !')
    print(f'Exception log stored in {str(Path(Path.home() / ".mpb" / "mpb.log"))}')


def _get_config(config_file):
    """Load a config file provided by the user"""
    try:
        with open(config_file, encoding='utf-8') as file:
            conf = {}
            try:
                conf = dict(yaml.safe_load(file))
            except yaml.YAMLError as exc:
                print(exc)
            return conf
    except FileNotFoundError:
        if config_file is not None:
            print(f'Can\'t open config file {config_file}')

    return {}


def get_config(config_file):
    """Load a config file provided by the user"""
    path = Path(config_file)
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config(config_file)

    path = Path("mpb.config")
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config("mpb.config")

    home_path = Path(Path.home() / '.mpb')
    path = Path(home_path / 'config')
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config(str(Path(home_path / 'config')))

    return {}


def is_package_config_valid(config):
    """Make basic check on the configuration file"""
    ret = config

    if isinstance(config, str):
        config = config.split()

    if isinstance(config, list):
        config = {p: {'src_type': 'distgit'} for p in config}
        ret = config

    if not isinstance(config, dict):
        print(
            f'Provided config file is malformed (packages is no dictionary: {type(config)})'
        )
        return None

    for pkg in config.keys():
        if config[pkg] == 'distgit':
            # Early adopters need to update their config files
            print(f'Config for {pkg} is malformed')
            ret = None
            break

        if 'src_type' not in config[pkg]:
            config[pkg]['src_type'] = 'distgit'

        src_type = config[pkg]['src_type']
        if src_type in ['file', 'script']:
            if 'src' not in config[pkg]:
                print(f'Config is missing source for {pkg}:{src_type}')
                ret = None
                break

            path = Path(config[pkg]['src'])
            if not path.is_file():
                print(
                    f'Path provided for {pkg}:{src_type} doesn\'t exists (got {str(path)})'
                )
                ret = None
                break
    return ret


def is_arch_valid(config):
    """Check if given archs are valid"""
    ret = True

    if 'arch' in config:
        config['archs'] = config['arch']

    if isinstance(config['archs'], str):
        config['archs'] = config['archs'].split()

    if isinstance(config['archs'], list):
        config['archs'] = set(config['archs'])

    if isinstance(config['archs'], dict):
        config['archs'] = set(config['archs'].keys())

    for arch in config['archs']:
        if arch not in authorized_archs:
            print(f'{arch} is not a supported architecture')
            ret = False

    return ret


def validate_config(config):
    """Validate configuration inputs"""
    if config['cancel'] and config['build_id'] <= 0:
        config['validity'] = False
        return
    if config['clean'] and config['build_id'] <= 0:
        config['validity'] = False
        return

    if any([config['clean'], config['cancel']]):
        return

    # Check all elements separately so that user can see all errors at once
    if config['verbose'] > 4:
        print('Checking for package configuration validity.')

    config['packages'] = is_package_config_valid(config['packages'])

    for ptype in ['list', 'append']:
        if 'revdeps' not in config:
            break

        if ptype not in config['revdeps']:
            continue

        config['revdeps'][ptype] = is_package_config_valid(config['revdeps'][ptype])

    if all([not config['packages'], config['build_id'] <= 0]):
        print('No package list nor build ID given')
        config['validity'] = False

    if all([config['stage'] >= 0, config['build_id'] <= 0]):
        print('Stage can\'t be given without a valid build ID.')
        config['validity'] = False

    if config['backend'] not in BACKENDS:
        config['validity'] = False
        print(f'Unknown back-end: {config["backend"]}')

    if not is_arch_valid(config):
        config['validity'] = False


def main():
    """Main function

    Setup argument parser, initialize default configuration, validates user
    input and execute the mass pre-build build.
    """
    # pylint: disable = global-statement
    global LOGGER
    global CURRENT_BACKEND

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--verbose',
        '-V',
        action='count',
        default=0,
        help='increase output verbosity, may be provided multiple times',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument(
        '--config',
        '-c',
        default='',
        help='''provide user specific config
                values in command line supersedes values in config file''',
    )
    parser.add_argument(
        '--stage',
        '-s',
        type=int,
        default=-1,
        help='stage to directly begin with (needs a valid build ID)',
    )
    parser.add_argument(
        '--buildid', '-b', type=int, default=-1, help='build ID to work on'
    )
    parser.add_argument(
        '--cancel',
        action='store_true',
        help='cancel any running package build from the given mass rebuild',
    )
    parser.add_argument(
        '--clean',
        action='store_true',
        help='cancel build and delete a given mass rebuild',
    )
    parser.add_argument(
        '--backend', default='copr', help='choose a different backend from copr default'
    )

    args = parser.parse_args()

    home_path = Path(Path.home() / '.mpb')

    logging.basicConfig(
        filename=str(Path(home_path / 'mpb.log')),
        filemode='a',
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
        datefmt='%H:%M:%S',
        level=logging.ERROR,
    )

    LOGGER = logging.getLogger(__name__)

    sys.excepthook = handle_exception
    threading.excepthook = handle_thread_exception

    # Populating default values
    config = {
        'backend': args.backend,
        'build_id': args.buildid,
        'stage': args.stage,
        'base_build_id': -1,
        'chroot': 'fedora-rawhide',
        'archs': {'x86_64'},
        'revdeps': {},
        'packages': {},
        'validity': True,
        'verbose': args.verbose,
        'cancel': args.cancel,
        'clean': args.clean,
        'data': str(Path(home_path / 'data')),
        'database': str(Path(home_path / 'share' / 'mpb.db')),
    }

    config |= get_config(args.config)

    validate_config(config)

    if config['validity'] is False:
        print('Configuration is broken.')
        sys.exit(1)

    if 0 < config['verbose'] <= 2:
        print(f'Using {config["backend"]} back-end')

    if config['verbose'] > 2:
        print('Using the following config: ')
        print(f'{yaml.dump(config, default_flow_style=False)}')

    data_path = Path(config['data'])
    data_path.mkdir(parents=True, exist_ok=True)

    CURRENT_BACKEND = BACKENDS[config['backend']](config)

    if config['cancel']:
        print(f'Canceling {config["build_id"]}')
        CURRENT_BACKEND.cancel()
        return

    if config['clean']:
        print(f'Cleaning build {config["build_id"]}')
        CURRENT_BACKEND.clean()
        return

    CURRENT_BACKEND.start_agents()

    while not CURRENT_BACKEND.is_completed():
        CURRENT_BACKEND.execute_stage()
        CURRENT_BACKEND.next_stage()

    CURRENT_BACKEND.stop_agents()


if __name__ == '__main__':
    main()
