# pylint: disable = missing-module-docstring
VERSION_INFO = (0, 1, '1')
VERSION = '.'.join(str(c) for c in VERSION_INFO)
