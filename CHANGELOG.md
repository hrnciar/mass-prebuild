# 0.1.1 (2022-08-22)

## Release Highlight

Moving out of the "alpha" naming, although there is still heavy development
ongoing. All the bugs spotted on alpha 1 are still not fixed, but there is
improvement already.

Starting from this release, Gitlab CI is enabled and will be improved over
time.

## Features

  - Log unhandled exceptions into a file
  - backend:copr: Allow user defined COPR configuration
  - CI: Enable
  - Build revdeps from a known state

## Fixes

  - whatrequires: Add support for pkgconfig
  - whatrequires: Improve support for Rawhide
  - backend: db: Close cursor after each transactions
  - Fix support for x86 and 'all' arches
  - backend:copr: Increase connection attempts

# 0.1.0-alpha1 (2022-06-28)

## Release Highlight

This is the first public release of the mass-prebuild tool.
Major changes from pre-alpha:
 - Architecture has been re-assessed
 - Backend implementation has been re-written

## Features

  - New back-end implementation
  - Rework reverse dependencies calculation
  - whatrequires: Add support for multiple architectures
  - Enable python packaging

## Fixes

  - Bump default timeout to 30 hours
  - Clean-up: Remove unnecessary files

## Doc

  - Rework architecture
